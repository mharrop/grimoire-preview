  const configPROV = {
    "mapwarper": {
      "https://mapwarper.prov.vic.gov.au/maps/11366": "Town of Melbourne (<a href='https://mapwarper.prov.vic.gov.au/maps/11366' target='_blank'>1837</a>).",
      "https://mapwarper.net/maps/83026": "Plan of Town of Melbourne, 1837 A.D. First Land Sales Held in Melbourne<br> on 1st June & 1st November 1837. (<a href='https://mapwarper.net/maps/83026' target='_blank'>1837</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/12544": "William Thomas map of westernport demonstration only see description (<a href='https://mapwarper.prov.vic.gov.au/maps/12544' target='_blank'>1840</a>).",
      "https://mapwarper.net/maps/82966": "Melbourne plan (<a href='https://mapwarper.net/maps/82966' target='_blank'>1840?</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/11390": "Plan of Eleven Allotments marked at St Kilda in the Parish of <br>South Melbourne (<a href='https://mapwarper.prov.vic.gov.au/maps/11390' target='_blank'>1845</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7323": "Victoria Parade (<a href='https://mapwarper.prov.vic.gov.au/maps/7323' target='_blank'>1851</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/11775": "Plan of the Extension of Melbourne called Carlton (<a href='https://mapwarper.prov.vic.gov.au/maps/11775' target='_blank'>1853</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/4327": "Contours from Contoured Plan of Part of the City of Melbourne (<a href='https://mapwarper.prov.vic.gov.au/maps/4327' target='_blank'>1853</a>).",
      
      "https://mapwarper.net/maps/83028": "The most complete popular and mercantile map of Melbourne (<a href='https://mapwarper.net/maps/83028' target='_blank'>1853?</a>).",
      
      "https://mapwarper.net/maps/82967": "Melbourne and its suburbs (<a href='https://mapwarper.net/maps/82967' target='_blank'>1855</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7304": "Sheet No. 1 of the Municipal District of East Collingwood (<a href='https://mapwarper.prov.vic.gov.au/maps/7304' target='_blank'>1856</a>).",
      "https://mapwarper.net/maps/80116": "Bibbs Map (<a href='https://mapwarper.net/maps/80116' target='_blank'>c1857</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7306": "Plan of the Streets and Buildings in East Collingwood (<a href='https://mapwarper.prov.vic.gov.au/maps/7306' target='_blank'>1858</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/5236": "Electoral District of East Bourke (<a href='https://mapwarper.prov.vic.gov.au/maps/5236' target='_blank'>1859</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7252": "Port of Melbourne extension to the City of Melbourne (<a href='https://mapwarper.prov.vic.gov.au/maps/7252' target='_blank'>1860</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7231": "Hobsons Bay and River Yarra (<a href='https://mapwarper.prov.vic.gov.au/maps/7231' target='_blank'>1864</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7316": "Plan of Melbourne, Hotham, Carlton, Fitzroy and East Melbourne (<a href='https://mapwarper.prov.vic.gov.au/maps/7316' target='_blank'>1866</a>).",
      "https://mapwarper.net/maps/82968": "Sands & McDougall's Melbourne directory map (<a href='https://mapwarper.net/maps/82968' target='_blank'>1869?</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/5507": "Victoria Skeleton Map of Telegraph Circuits (<a href='https://mapwarper.prov.vic.gov.au/maps/5507' target='_blank'>1870</a>).",
      "https://mapwarper.net/maps/82969": "New plan of Melbourne and suburbs (<a href='https://mapwarper.net/maps/82969' target='_blank'>1874</a>).",
      "https://mapwarper.net/maps/82970": "The Commercial Exchange map of the city Melbourne (<a href='https://mapwarper.net/maps/82970' target='_blank'>1877</a>).",
      "https://mapwarper.net/maps/82971": "Plan of Melbourne showing proposed new railway station (<a href='https://mapwarper.net/maps/82971' target='_blank'>1878</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/4215": "Melbourne Harbour Trust General Plan Shewing Harbour Improvements (<a href='https://mapwarper.prov.vic.gov.au/maps/4215' target='_blank'>1879</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/4331": "Contours from Contour Plan of Portion of the City of Melbourne (<a href='https://mapwarper.prov.vic.gov.au/maps/4331' target='_blank'>1880</a>).",
      "https://mapwarper.net/maps/82972": "Melbourne (<a href='https://mapwarper.net/maps/82972' target='_blank'>1880</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7981": "Melbourne and Suburban Lines (<a href='https://mapwarper.prov.vic.gov.au/maps/7981' target='_blank'>1882</a>).",
      "https://mapwarper.net/maps/82973": "Plan of Melbourne (<a href='https://mapwarper.net/maps/82973' target='_blank'>1882</a>).",
      "https://mapwarper.net/maps/82975": "Plan No. 27 East Melbourne (<a href='https://mapwarper.net/maps/82975' target='_blank'>1895?</a>).",
      "https://mapwarper.net/maps/82976": "Plan No. 25 City of Melbourne (<a href='https://mapwarper.net/maps/82976' target='_blank'>1895?</a>).",
      "https://mapwarper.net/maps/82977": "City of Melbourne 1907, Victoria, Australia (<a href='https://mapwarper.net/maps/82977' target='_blank'>1907</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7265": "Hobsons Bay and River Yarra (<a href='https://mapwarper.prov.vic.gov.au/maps/7265' target='_blank'>1908</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/6188": "Werribee Park Estate (<a href='https://mapwarper.prov.vic.gov.au/maps/6188' target='_blank'>1908</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/6195": "Werribee Park Estate (<a href='https://mapwarper.prov.vic.gov.au/maps/6195' target='_blank'>1909</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/7342": "City of Melbourne Block Plan compiled for use in City Surveyor's office (<a href='https://mapwarper.prov.vic.gov.au/maps/7342' target='_blank'>1910</a>).",
      "https://mapwarper.prov.vic.gov.au/maps/10951": "Sketchmap of Country Around Oakleigh (<a href='https://mapwarper.prov.vic.gov.au/maps/10951' target='_blank'>1913</a>).",
      "https://mapwarper.net/maps/82978": "Map of Melbourne : for the use of members of the British Association <br>&nbsp;for the Advancement of Science, Australian Meeting (<a href='https://mapwarper.net/maps/82978' target='_blank'>1914</a>).",

      "https://mapwarper.net/maps/83030": "Victoria Insurance Map Company. The City of Melbourne Number Map (<a href='https://mapwarper.net/maps/83030' target='_blank'>1915</a>).",
      
      "https://mapwarper.net/maps/82979": "Sands and McDougall Pty. Ltd. New Map of Melbourne and Suburbs (<a href='https://mapwarper.net/maps/82979' target='_blank'>1920?</a>).",
      "https://mapwarper.net/maps/82982": "The Star Plan of City of Melbourne Sketch of Melbourne City Business Area (<a href='https://mapwarper.net/maps/82982' target='_blank'>1934</a>).",

      "https://mapwarper.net/maps/83035": "Morgan's Official Street Directory, Melbourne & Suburbs, Map 1, part 1 (<a href='https://mapwarper.net/maps/83035' target='_blank'>1935</a>).",
      "https://mapwarper.net/maps/80120": "Morgan's Official Street Directory, Melbourne & Suburbs, Map 1, part 2 (<a href='https://mapwarper.net/maps/80120' target='_blank'>1935</a>).",

      
      "https://mapwarper.net/maps/83034": "Morgan's Official Street Directory, Melbourne & Suburbs, Map 53 (<a href='https://mapwarper.net/maps/83034' target='_blank'>1935</a>).",

      


      "https://mapwarper.prov.vic.gov.au/maps/7452": "Geological Map of Melbourne and Suburbs (<a href='https://mapwarper.prov.vic.gov.au/maps/7452' target='_blank'>1937</a>).",
      "https://mapwarper.net/maps/82904": "Melbourne Aerial Photography (<a href='https://mapwarper.net/maps/82904' target='_blank'>1945</a>).",
      "https://mapwarper.net/maps/82906": "Melbourne Aerial Photography (<a href='https://mapwarper.net/maps/82906' target='_blank'>1945</a>).",

      "https://mapwarper.net/maps/83031": "Morgan's Official street directory. Melbourne & Suburbs. Map 1 (<a href='https://mapwarper.net/maps/83031' target='_blank'>1951</a>).",
      "https://mapwarper.net/maps/83032": "Morgan's Official street directory. Melbourne & Suburbs. Map 1a (<a href='https://mapwarper.net/maps/83032' target='_blank'>1951</a>).",
      "https://mapwarper.net/maps/83033": "Morgan's Official street directory. Melbourne & Suburbs. Map 53 (<a href='https://mapwarper.net/maps/83033' target='_blank'>1951</a>)."
    },
    "mapCentre": "-37.840935, 144.946457",
    "mapZoom": "11",
    "mapIcons": {
      "default": "place",
      "musicandmusicalinstruments": "storefront",
      "musicandmusicalinstrumentsellers": "storefront",
      "musicians": "person",
      "musicsellers": "storefront",
      "musicvenues": "nightlife",
      "organbuilders": "design_services",
      "pianoandplayerpianoimporters": "place",
      "pianoandplayerpianoimportersandmanufacturers": "place",
      "pianodealers": "place",
      "pianofortemakers": "design_services",
      "pianofortemakersanddealers": "place",
      "pianofortemakersandrepairers": "place",
      "pianofortemakersdealersandturners": "place",
      "pianofortetuners": "engineering",
      "pianoimporters": "sailing",
      "pianomanufacturers": "design_services",
      "pianotunersandrepairers": "engineering"
    }
  };