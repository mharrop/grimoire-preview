<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>Grimoire for the Digital Humanities: The Forbidden Atlas</title>
    <link rel="stylesheet" href="../style.css">
  </head>
  <body>
<div class="main">  
    <h1><i>Grimoire for the Digital Humanities: <br/>The Forbidden Atlas</i></h1>
    <h1>2.1 Basic Magic Tricks</h1>
    <h3>Authors: Dr Mitchell Harrop, Dr Jon Garber</h3>
    
    <p>
    This chapter is a gentle introduction to some of the tools, concepts and magic ingredients needed in later chapters to create maps. 
    The chapter includes an introduction to structured data. An appreciation of structured data is needed in order to be able to create the kinds 
    of files that mapping software often uses to render great visualisations. This section is followed by a reminder about spreadsheet concepts...
    <span class="preview_fade">... PREVIEW ONLY ...</span>
    </p>
    
    <p>
    <span class="preview_fade">... PREVIEW ONLY ...</span>
    ... quickest and easiest method to save information was to write each part of the information to a file, one after the other, like so:
    </p>
    

<pre><code>1, Tilly, Aston, 100, 1 Bourke Street Melbourne,
2, Dr Vera, Brown, 1000, 123 Swanston Street Melbourne,
3, Annette, Crawford, 101, 246 Elizabeth Street Melbourne,
4, … etc.</code></pre>

<p>
And there would be some corresponding code written by a Software Engineer to read the file back into the system and do something useful with the information. 
Code to open files, code to save ... 
<br>
<span class="preview_fade">... PREVIEW ONLY ...</span>
</p>


<p>
If you were to open the file in a spreadsheet the last element would get a single column rather than be spread over multiple columns, i.e.:
</p>

<table class="spreadsheet">
    <tr>
        <th>&nbsp;</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Tragedy</td>
        <td>-3.4</td>
        <td>1.0198</td>
        <td>[-4,-4,-4,-4,-2,-3,-1,-4,-4,-4]</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Villains</td>
        <td>-3.4</td>
        <td>0.91652</td>
        <td>[-4,-3,-4,-3,-4,-3,-4,-4,-1,-4]</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Torturers</td>
        <td>-3.5</td>
        <td>0.67082</td>
        <td>[-4,-4,-3,-3,-4,-4,-4,-2,-3,-4]</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Hell</td>
        <td>-3.6</td>
        <td>0.66332</td>
        <td>[-4,-4,-4,-4,-4,-2,-3,-4,-3,-4]</td>
    </tr>
    <tr>
        <td>...</td>
        <td>...</td>
        <td>...</td>
        <td>...</td>
        <td>...</td>
    </tr>
</table>

<p>
Column D is one column rather than multiple columns for each of the numbers inside the square brackets, despite the numerical values being separated by commas. This is
because the file is a Tab Separated file, not a ...<span class="preview_fade">... PREVIEW ONLY ...</span>
</p>

<p>
... <span class="preview_fade">... PREVIEW ONLY ...</span><br>
not a perfect job, as we don’t know, for example, the currency of the <code>BankBalance</code>. Thus a business probably won’t collapse 
when the curse manifests because the next Software Engineer can (more) easily pick up the mantle and understand more or less what on earth 
is going on by looking at the first line. 
</p>

<div class="STEWARDSHIP breakout_box">
    <div class="STEWARDSHIP_heading breakout_box_heading">
        <h2>STEWARDSHIP - DATA DICTIONARIES</h2> 
        <h3>Author: Karen M Thompson</h3>
    </div>
    <div class="breakout_box_body">
        <figure>
            <img width="300px" src="../images/lamp.jpg" alt="Lamp (Melbourne History Resources, 2019a)" >
            <figcaption>Image source: Melbourne History Resources (2019a)</figcaption>
        </figure>
        <p>
        Hello, I’m Karen. In these boxes we will talk about Data Stewardship, which is the role of maximising the value of data for research purposes. These Stewardship boxes are an aside that keep an eye on the bigger research picture. 
        </p>
        <p>
        Good data stewardship is your responsibility as a researcher - only you can prevent poor data stewardship. 
        </p>
        <p>
        Why have we assigned an image of the lamps outside Parliament House in Melbourne as the visual marker for these boxes? Well, good data stewardship lights the way for other researchers, and your future self, to make best use of your research data. And if the lights go out, and it's dark, it sometimes can feel a bit spooky and you may temporarily lose your way. So lights felt appropriate.
        </p>
        <p>
        In this first Stewardship box we’re considering ‘Data Dictionaries’. 
        </p>
        <p>
        You should consider building Data Dictionaries for your work. This is basically an explanation of each of the columns in your spreadsheet or database: its unique name (best not to have two things with the same, or easily confused, names); the meaning; if it’s optional or absolutely required; format; allowable values; if it’s raw data or made up from other data components; and more. It can be a simple sheet in your spreadsheet which describes the contents of another sheet. With more complex data, you’d also include descriptions of how each column relates to others. 
        </p>
    </div>
    
</div>

<p>
But let’s consider the limitations of this flavour of in-built documentation for the second file example. 
The second file doesn’t need a first line specifying the names of each column of data because there just happens 
to be a peer reviewed paper (Hutto and Gilbert, 2014) describing in great detail all aspects of the system that uses this file.
Unlike the CEO exercising poor management skills and demanding that the engineers consistently make tight deadlines and fully document everything at all times and do a million other things (sigh), the peer reviewed output is the deadline. The peer reviewed paper makes for great documentation.
</p>

<p>
But what would the headings for the second file be? Having read the Hutto and Gilbert paper, perhaps something like this:
</p>

<pre><code><strong>Word &rarr; Score &rarr; Variance &rarr; HumanScores</strong>
Magnificently &rarr; 3.4 &rarr; 0.66332 &rarr; [3, 3, 3, 4, 4, 2, 4, 4, 3, 4]
Ecstasy &rarr; 3.3 &rarr; 1.18743 &rarr; [4, 4, 3, 4, 4, 0, 3, 3, 4, 4]
... etc.</code></pre>

<p>
If you were encountering such a file for the first time you might not be able to figure out what the <code>HumanScores</code> are all about. 
It rightly took a lot of ink to explain the concept in the Hutto and Gilbert paper. According to the paper, these are scores that humans 
made for each of the words in the file, ranging from negative to positive and representing the sentiment associated with them. 
That probably doesn’t make much sense if you haven’t read the paper. Just trust us. Or go read the Hutto and Gilbert paper yourself. 
Reading well into the paper one can find that the <code>Variance</code> was calculated from the <code>HumanScores</code>. But the <code>HumanScores</code> isn’t used anywhere 
within the code that uses this file! It is simply there for completeness and contestability by other researchers. Just imagine trying to work
out what <code>HumanScores</code> means without access to or knowledge of the peer reviewed paper. You wouldn’t be able to find any use 
of <code>HumanScores</code> in the software because it isn’t used:<br>
<span class="preview_fade">... PREVIEW ONLY ...</span>
</p>

<p>
The <code>nameEntry</code> element contains two part elements. The <code>part</code> elements are said to be the children of the <code>nameEntry</code> parent. 
This particular standard uses attributes (e.g. <code>localType="familyname"</code>) to define the data. Looking at the first <code>part</code> element, it has 
an attribute of <code>localType</code> which has the value of <code>familyname</code>. Inside the element is <code>Barry</code>, the last name of the person in question. 
Looking at the second <code>part</code> element, it has an attribute of <code>givenname</code>. The element contains the first name of the person in question (<code>Redmond</code>).
The standard this file uses defines what constitutes a family name and what constitutes a given name. Again, don’t worry if all this terminology goes in one ear and 
out the other. We’ll remind you on an as-needed basis.
</p>

<div class="breakout_box YOUR_TURN">
    <div class="breakout_box_heading YOUR_TURN_heading">
        <h2>YOUR TURN</h2>
    </div>
    <div class="breakout_box_body">
        <ul>
            <li>What other information is in the example above? Read through it. </li>
            <ul>
                <li>When did Redmond Barry live? </li>
                <li>Where was he born? </li>
                <li>Where did he die? </li>
                <li>What was his occupation?</li>
            </ul>
            <li><i>See, it’s not so scary when you look at individual elements and take the time to see what’s going on.</i></li>
        </ul>
        
    </div>
</div>

<p>
However, there could just as easily be a different standard which, instead of ...<br><span class="preview_fade">... PREVIEW ONLY ...</span></p>



    <h1>2.3 A Gentle Reminder about Spreadsheets</h1>

<p>
Spreadsheets are the original “killer app”. They are the software that prompted people and businesses to go out and get their very own personal computers. They were and still are a revolution. Although not a very exciting revolution.
</p>
<p>
This Grimoire does NOT use Microsoft Excel. You need to pay for Microsoft Excel or be a member of an institution that pays for you. Instead, we use the free Google Sheets in our examples. Google Sheets and Excel are very comparable and compatible spreadsheet programs, but with a few quirky differences. As such, we endeavour, wherever possible, to be spreadsheet agnostic in examples. The examples we provide will often work without any changes in Microsoft Excel. 
</p>
<p>
Google Sheets exists within Google Docs. Google Docs is Google’s answer to the Microsoft Office Suite. Instead of a Microsoft Word document, there’s a Google Doc. Instead of Microsoft Excel, there’s Google Sheets. PowerPoint? Google Slides. And so on. It’s an invasion of doppelgangers. But instead of being a desktop application you install, Google Docs runs in your web browser, so you can use the programs anywhere that you’ve got a computer and the internet. Brilliant, huh? Furthermore, the files are all saved in the cloud, also known as Google Drive. You need an account on Google and to activate Google Sheets. You can sign up here or use a gmail account if you already have one:
</p>
<blockquote>
    <a href="https://www.google.com.au/docs/about/">https://www.google.com.au/docs/about/</a>
</blockquote>
<p>
Now it’s time for a quick refresher on the general spreadsheet skills you ought to have coming into this book. If you already use spreadsheets extensively in your day to day work, you can go ahead and skip the rest of this section.
</p>
<p>
Recall spreadsheets have rows and columns. The rows have numbers and the columns have letters:
</p>



<table class="spreadsheet">
    <tbody><tr>
        <th>&nbsp;</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Wands</td>
        <td>Broomsticks</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>2</td>
        <td>100</td>
        <td>200</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>3</td>
        <td>350</td>
        <td>450</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</tbody></table>


<p>
Recall that there are cells. A1 is a cell. It contains the value <code>Wands</code> in the example above. Cell B1 contains the value <code>Broomsticks</code>. 
Just like the game Battleships, right? Cell A2 is <code>100</code>. To make sure you are paying attention, what’s in cell B3?
</p>
<p>
Yep, <code>450</code>. You sunk my broomstick. 
</p>
<p>
Spreadsheets can have multiple sheets. Sometimes a sheet is called a worksheet or workbook. Generally, access to multiple sheets is via tabs that are found at the bottom of the interface, in this example this is where it says Sheet 3:
</p>


<table class="spreadsheet">
    <tbody><tr>
        <th>&nbsp;</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
        <th>E</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Crystal Ball</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>2</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>3</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>...</td>
        <td>...</td>
        <td>...</td>
        <td>...</td>
        <td>...</td>
        <td>...</td>
    </tr>
    <tr class="sheet">
        <td colspan="2">&nbsp;</td>
        <td>Sheet 1</td>
        <td>Sheet 2</td>
        <td class="sheet_selected">Sheet 3</td>
        <td>&nbsp;</td>
    </tr>
    
</tbody></table>

<p>
Recall that spreadsheets have functions and formulas for doing things like calculations and that they start with an equal ( = ) sign:
</p>

<table class="spreadsheet">
    <tbody><tr>
        <th>&nbsp;</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Wands</td>
        <td>Broomsticks</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>2</td>
        <td>100</td>
        <td>200</td>
        <td>=A1+B2</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>3</td>
        <td>350</td>
        <td>450</td>
        <td>=SUM(A3,B2,A4)</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>4</td>
        <td>575</td>
        <td>675</td>
        <td>=AVERAGE(A2:A4)</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>5</td>
        <td>...</td>
        <td>...</td>
        <td>...</td>
        <td>...</td>
    </tr>    
</tbody></table>

<p>
The formula in cell C2 above adds the values in cells A1 and B2 together. As such, the values of <code>100</code> and <code>200</code> are added together. 
The formula runs when you hit enter after typing in the formula. Cell C1 will then appear to be <code>300</code>, but the formula will still be there, 
underneath it all, waiting to strike. <br><span class="preview_fade">... PREVIEW ONLY ...</span>
</p>


<p>
    Next: <a href="../contours/">3. The Ley Lines and Contours of Melbourne</a>
</p>







</div>
</body>
</html>